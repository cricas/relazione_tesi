#link utili per saper come scrivere 
https://en.wikibooks.org/wiki/LaTeX/Fonts#Shapes

\texttt{Host: www.unibo.it} --> come fare un testo in MONO
$\rightarrow$               --> come fare una freccia a destra

inserire un'immagine
\includegraphics[width=100mm]{1_1}


per visual code andare in impostazioni cercare word wrap --> attivare WordWrapColumn
impostare poi editor a 120 per avere delle righe sempre lunghe 120 caratteri lato codice

\setlist{nolistsep}
\begin{description}                     %crea un elenco descrittivo
    \item[interfaccia]: definisce le operazioni elementari e i servizi che un livello rende disponibile al livello soprastante;
    \item[Peer]: le entità che formano i livelli di pari grado sono chiamati peer. I peer possono essere processi software, dispositivi hardware o anche esseri umani;
    \item[Protocollo]: sono le regole che permettono la comunicazione tra i diversi peer. 
  \end{description}



  bibliografia

  protocollo IP
  \\\\\\https://www.cloudflare.com/it-it/learning/network-layer/internet-protocol/

  \\\\\\\\\https://www.ionos.it/digitalguide/server/know-how/che-cose-il-protocollo-internet-definizione-di-ip-co/

DHCP
\\\\\\\https://cdc.roma2.infn.it/node/51
\\\\\\\https://it.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol

  protocollo mac
  \\\\\\\\https://www.sciencedirect.com/topics/computer-science/data-link-layer

\\\\\chiave simmetrica wiki cifrai a blocco wiki
\\\\chiave pubblica wiki

sicuraza informaitca
\\\https://www.zerounoweb.it/techtarget/searchdatacenter/la-crittografia-quando-nasce-come-funziona-perche-e-alleata-della-sicurezza-informatica/
\\\https://it.wikipedia.org/wiki/Crittografia
\\\https://bitmind.it/web/riservatezza-autenticita-integrita-disponibilita-e-non-ripudio/

TLS 
\\\\https://docs.italia.it/AgID/documenti-in-consultazione/lg-sicurezza-interoperabilita-docs/it/bozza/doc/01_Raccomandazioni%20TLS/04_tls.html
\\\https://www.cloudflare.com/it-it/learning/ssl/what-happens-in-a-tls-handshake/
\\\\https://www.cloudflare.com/it-it/learning/ssl/how-does-ssl-work/

come rendere sicuri i protoccoli
\\https://www.cloudflare.com/it-it/learning/dns/dns-over-tls/
\\https://en.wikipedia.org/wiki/DNS_over_TLS


virtual box
\\https://www.virtualbox.org/manual/ch01.html
\\\https://www.virtualbox.org/manual/ch06.html
\\\https://blog.smsoft.it/2012/10/16/come-funziona-la-rete-su-virtualbox/

wireshark
\\https://www.wireshark.org/

tcpdump 
\\https://www.tcpdump.org/manpages/tcpdump.1.html
\\https://it.wikipedia.org/wiki/Tcpdump

ping 
\\https://it.wikipedia.org/wiki/Ping
\\\https://www.ionos.it/digitalguide/server/tools-o-strumenti/il-comando-ping/

tracert 
\\https://support.microsoft.com/it-it/topic/utilizzo-di-tracert-per-risolvere-i-problemi-relativi-al-protocollo-tcp-ip-in-windows-e643d72b-2f4f-cdd6-09a0-fd2989c7ca8e

arpspoofing
\\https://en.wikipedia.org/wiki/ARP_spoofing

ettercup 
\\https://it.wikipedia.org/wiki/Ettercap

capitolo 3
\\https://www.varonis.com/blog/arp-poisoning/ -->spiegazione di dos
\\https://www.cybersecurity360.it/nuove-minacce/phishing-cose-e-come-proteggersi-la-guida-completa/ --> phising
\\https://www.studiocataldi.it/guide_legali/pillole/il-phishing-un-illecito-civile-e-penale.asp --> phissing
\\https://it.wikipedia.org/wiki/Ingegneria_sociale
\\https://usa.kaspersky.com/resource-center/definitions/what-is-social-engineering

gateway
\\https://www.tdblog.it/gateway-che-cose-come-funziona-e-perche-oggi-e-fondamentale/

spoofing DHCP
\\https://www.ionos.it/digitalguide/server/sicurezza/dhcp-snooping/

spoofing ARP
\\https://www.veracode.com/security/arp-spoofing

proxy 
\\https://www.pandasecurity.com/it/mediacenter/tecnologia/proxy-cose-e-come-si-configura/




//////////////////////////////////////////code da fare per il capitolo 4
la prima immagine quella della rete reale e fisica mancano i due punti nel pc target wind non virtuale
poi la scrita non virtuale deve scomparire per lascire il posto alla scritta reale